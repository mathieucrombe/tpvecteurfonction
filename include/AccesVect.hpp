// Fichier "AccesVect.hpp"
// Programme contenant les signatures des fonctions
// PICHART Antonin - CROMBE Mathieu

//---------------------------------------------------------------------------------------------------------------

#pragma once

float CalculerMoy(float prmVect[], int prmTaile); // Signature de la fonction  "CalculerMoy()"

void Initialiser(int prmVect[], int prmTaille, int prmVal); // Signature de la fonction "Initialiser()"

void CopierVecteur(int prmVectDest[], int prmVectSrc[], int prmN); // Signature de la fonction "CopierVecteur()"