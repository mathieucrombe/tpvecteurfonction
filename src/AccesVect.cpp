// Fichier "AccesVect.cpp"
// Programme de codage des fonctions
// PICHART Antonin - CROMBE Mathieu

//---------------------------------------------------------------------------------------------------------------

#include "AccesVect.hpp"

//-----------------------------------------------------------------------------

float CalculerMoy(float prmVect[], int prmTaille)
{
    float moy = 0;           // Déclaration de la variable moy en flottant
    float valRetour = -1000; // Déclaration de la variable valRetour en flottant et initialiser à -1000;

    if (prmTaille != 0)
    {
        for (int i = 0; i < prmTaille; i++) // Boucle "pour" sur les valeur du vecteur
        {
            moy = moy + prmVect[i]; // Addition des variables du vecteur
        }
        valRetour = moy / prmTaille; // Division du résultat de l'addition par le nombre de valeurs
    }
    return valRetour; // Retourne la valeur valRetour
}

//-----------------------------------------------------------------------------

void Initialiser(int prmVect[], int prmTaille, int prmVal)
{
    for (int i = 0; i < prmTaille; i++) // Boucle "pour" sur les valeur du vecteur
    {
        prmVect[i] = prmVal; // Initialisation de prmVect[i] en prmVal
    }
}

//-----------------------------------------------------------------------------

void CopierVecteur(int prmVectDest[], int prmVectSrc[], int prmN)
{
    for (int i = 0; i < prmN; i++) // Boucle "pour" sur les valeur du vecteur
    {
        prmVectDest[i] = prmVectSrc[i]; // Initialisation de prmVectDest[i] en prmVectSrc[i]
    }
}