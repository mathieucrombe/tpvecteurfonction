// Fichier "main.cpp"
// Programme de test des fonctions
// PICHART Antonin - CROMBE Mathieu

//---------------------------------------------------------------------------------------------------------------

#include <iostream> // Pour : cout, endl
using namespace std ;
#define NB_CASES 12 // Taille du vecteur

#include <AccesVect.hpp> // Déclaration de la fonction

int main()
{
    // Exercice 1

    /* 
    float vectTest[NB_CASES] = {10.5, 11.3, 12.8, 15.2, 15.1, 13.7, 10.1, 8.6, 5.0, 2.3};           // vecteur de test
    float moyenne = 0;              // Déclaration d'une variable local "moyenne"

    //Calcul de la moyenne des valeurs contenues dans le vecteur avec la fonction CalculerMoy()
    //Moyenne sur 1 valeur, 2 valeurs, 10 valeurs, 0 valeur, 15 valeurs (débordement)

    moyenne = CalculerMoy(vectTest, 1) ;            // Moyenne = 10.5
    moyenne = CalculerMoy(vectTest, 2) ;            // Moyenne = (10.5 + 11.3)/2 = 10.9
    moyenne = CalculerMoy(vectTest, 10) ;           // Moyenne = 104.6 / 10 = 10.46
    moyenne = CalculerMoy(vectTest, 0) ;            // erreur : valeur de retour = -1000
    moyenne = CalculerMoy(vectTest, 15) ;           // erreur de calcul : débordement du vecteur 
    */

//-----------------------------------------------------------------------------

    // Exercice 2

    /*
    int vectTest[NB_CASES] ;                        // Déclaration du vecteur vectTest[NB_CASES]

    for (int i = 0; i < NB_CASES; i++) vectTest[i] = 0;

    Initialiser(vectTest, NB_CASES, 5) ;            // initialiser toutes les cases avec la valeur 5
    Initialiser(vectTest, 4, -1) ;                  // initialiser les 4 premières cases avec la valeur -1
    */

//-----------------------------------------------------------------------------

    // Exercice 3

    int vectSource[NB_CASES];           // vecteur source
    int vectDestination[NB_CASES];      // vecteur de destination
    int entier1 = 111111;               // Pour les test de débordement
    int entier2 = 222222;               // Pour les test de débordement

    // Initialisation des vecteurs
    for (int i = 0; i < NB_CASES; i++)
    {
        vectDestination[i] = -1;
        vectSource[i] = i + 100;
    }

    // test de copie
    CopierVecteur(vectDestination, vectSource, 0) ;                 // pas de copie
    CopierVecteur(vectDestination, vectSource, 1) ;                 // copie d'une valeur
    CopierVecteur(vectDestination, vectSource, 5) ;                 // copie de 5 valeurs
    CopierVecteur(vectDestination, vectSource, NB_CASES) ;          // copie du vecteur complet
    CopierVecteur(vectDestination, vectSource, NB_CASES + 2) ;      // débordement

    cout << "adresse de vectdest " << hex << vectDestination << endl ;
    cout << "adresse de vectsource " << hex << vectSource << endl ;
    cout << "adresse de entier1 " << hex << &entier1 << endl ;
    cout << "adresse de entier2 " << hex << &entier2 << endl ;
}